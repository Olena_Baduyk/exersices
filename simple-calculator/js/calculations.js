var action;

function Operations(num1, num2, action) {
    switch(action) {
        case 1: 
            return num1 * num2;
         
        case 2: 
            return num1 / num2;

        case 3: 
            return Math.pow(num1, num2);

        default:
            text = 'Type in the numbers to do the calculations';
    }
}

function Calculation(){
    var firstNumber = document.getElementById('firstNumber');
    var num1 = parseFloat(firstNumber.value);

    var secondNumber = document.getElementById('secondNumber');
    var num2 = parseFloat(secondNumber.value);

    var displayResult = document.getElementById('display');
    displayResult.textContent = Operations(num1, num2, 1);

}
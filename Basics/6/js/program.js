function myFunction(){
    var year = parseFloat(document.getElementById('year').value);
    var result = document.getElementById('result');

    var y = year % 4;

    if(y === 0){
        result.textContent = year + ' is a leap year';
    }
    else{
        result.textContent = year + ' is not a leap year';
    }    
}

function reset(){
    var a = document.getElementById('year');
        a.value = '';
    var b = document.getElementById('result');
        b.innerHTML = '';
}
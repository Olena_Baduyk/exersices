function calculation() {
    var side1 = parseFloat(document.getElementById('side1').value);
    var side2 = parseFloat(document.getElementById('side2').value);
    var side3 = parseFloat(document.getElementById('side3').value);
    
    var perimeter = (side1 + side2 + side3) * 0.5;
    var result = Math.round(Math.sqrt(perimeter*((perimeter-side1)*(perimeter-side2)*(perimeter-side3))));

    var elResult = document.getElementById('result');
    elResult.textContent = 'Triangle area is: ' + result;
}

function resetInput() {
    var a = document.getElementById('side1');
        a.value = '';
    var b = document.getElementById('side2');
        b.value = '';
    var c = document.getElementById('side3');
        c.value = '';
    var d = document.getElementById('result');
        d.innerHTML = '';
}
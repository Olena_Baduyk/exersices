var day = new Date();
var daysNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
document.getElementById('currentDay').innerHTML = daysNames[day.getDay()];

var hoursNames = ['0','1','2','3','4','5','6','7','8','9','10','11','00','1','2','3','4','5','6','7','8','9','10','11','00',];

if(day.getHours() < 13){
    document.getElementById('currentTime').innerHTML = hoursNames[day.getHours()] + ' AM : ' + day.getMinutes() + ' : ' + day.getSeconds();
}
else{
    document.getElementById('currentTime').innerHTML = hoursNames[day.getHours()] + ' PM : ' + day.getMinutes() + ' : ' + day.getSeconds();
}